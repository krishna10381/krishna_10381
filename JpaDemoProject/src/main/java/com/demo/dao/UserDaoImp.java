package com.demo.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.demo.entity.User;

@Repository
public class UserDaoImp implements UserDao {

	private SessionFactory sessionFactory;
	@Override
	public void add(User user) {
		sessionFactory.getCurrentSession().save(user);
		
	}

	@Override
	public List<User> listUsers() {
		@SuppressWarnings("unchecked")
		TypedQuery<User> query=sessionFactory.getCurrentSession().createQuery("from User");
	      return query.getResultList();
	}

}
