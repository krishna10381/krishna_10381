package com.demo.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.dao.UserDao;
import com.demo.entity.User;

@Service
public class UserServiceImp implements UserService {

	@Autowired
	private UserDao userDao;
	@Transactional
	@Override
	public void add(User user) {
		 userDao.add(user);
		
	}
	@Transactional
	@Override
	public List<User> listUsers() {
		
		return userDao.listUsers();
		
	}


}
