package com.sample.service;

import java.util.List;

import com.sample.bean.Student;
import com.sample.dao.StudentDAO;
import com.sample.dao.StudentDAOImpl;

public class StudentServiceImpl implements StudentService {

	/**
	 * This method calls the createStudent method presented in DAO class
	 * 
	 * @param student obj
	 * @return boolean
	 */
	@Override
	public boolean insertStudent(Student student) {
		// TODO Auto-generated method stub
		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.createStudent(student);
		return result;
	}

	/**
	 * This method calls the serachById method presented in DAO class
	 * 
	 * @param id
	 * @return student obj
	 */
	@Override
	public Student searchStudent(int id) {
		StudentDAO studentDAO = new StudentDAOImpl();
		Student student = studentDAO.searchById(id);
		return student;
	}

	/**
	 * This method calls the getAllStudent method presented in DAO class
	 * 
	 * @return List student
	 */
	@Override
	public List<Student> fetchAllStudents() {
		StudentDAO studentDAO = new StudentDAOImpl();
		List<Student> studentList = studentDAO.getAllStudents();
		return studentList;
	}

	/**
	 * This method calls the deleteStudent method presented in DAO class
	 * 
	 * @param id
	 * @return boolean
	 */
	@Override
	public boolean deleteStudent(int id) {
		StudentDAO studentDAO = new StudentDAOImpl();
		boolean result = studentDAO.deleteStudent(id);
		return result;
	}

	/**
	 * This method calls the updateStudent method presented in DAO class
	 * 
	 * @param student obj
	 * @return int
	 * 
	 */
	@Override
	public int updateStudent(Student student) {
		StudentDAO studentDAO = new StudentDAOImpl();
		int result = studentDAO.updateStudent(student);
		return result;
	}

}
