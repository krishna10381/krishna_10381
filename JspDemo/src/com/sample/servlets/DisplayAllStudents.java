package com.sample.servlets;

import java.io.IOException;

import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class DisplayAllStudents
 */
@WebServlet("/DisplayAllStudents")
public class DisplayAllStudents extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * This method prints the all data of students that is found in student table
	 * 
	 * @param request,response
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("DisplayAllStudents.jsp");
		if (request.getSession().getAttribute("username") == null) {
			request.setAttribute("msg", "Please Login..");
		} else {
			StudentService service = new StudentServiceImpl();
			List<Student> studentList = service.fetchAllStudents();
			request.setAttribute("studentList", studentList);
			rd.forward(request, response);
		}
	}

}
