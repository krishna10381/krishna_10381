package com.sample.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * This method finds the student data with student id and deletes the following
	 * data
	 * 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		int studentId = 0;
		RequestDispatcher rd = request.getRequestDispatcher("StudentRegistration.jsp");

		if (request.getSession().getAttribute("username") == null) {
			request.setAttribute("msg", "Please Login..");
			rd.forward(request, response);
		} else {
			try {
				studentId = Integer.parseInt(request.getParameter("id"));
				StudentService service = new StudentServiceImpl();
				boolean student = service.deleteStudent(studentId);

				if (student == true) {
					request.setAttribute("msg", "Deleted Successfully");
					rd.forward(request, response);
				} else {
					request.setAttribute("msg", "Student Doesn't Exist");
					rd.forward(request, response);

				}

			} catch (Exception e) {
				request.setAttribute("msg", " Error: Enter The Values");
				rd.forward(request, response);
			}
		}
	}
}
