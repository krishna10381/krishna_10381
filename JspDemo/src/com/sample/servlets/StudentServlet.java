package com.sample.servlets;

import java.io.IOException;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/StudentServlet")
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * This method takes the input and insert the data into the student table
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("StudentRegistration.jsp");
		int studentId = 0;
		String studentName = null;
		int studentAge = 0;

		if (request.getSession().getAttribute("username") == null) {
			request.setAttribute("msg", "Please Login");
			rd.forward(request, response);
		} else {
			try {
				studentId = Integer.parseInt(request.getParameter("id"));
				studentName = request.getParameter("name");
				studentAge = Integer.parseInt(request.getParameter("age"));
				Student student = new Student();
				student.setId(studentId);
				student.setName(studentName);
				student.setAge(studentAge);
				StudentService service = new StudentServiceImpl();
				boolean result = service.insertStudent(student);
				if (result) {
					request.setAttribute("msg", "Inserted Successfully");
					rd.forward(request, response);
				} else {
					request.setAttribute("msg", "Error Occured please Try Again");
					rd.forward(request, response);
				}
			} catch (Exception np) {
				request.setAttribute("msg", "Please Login");
				rd.forward(request, response);
			}
		}
	}
}
