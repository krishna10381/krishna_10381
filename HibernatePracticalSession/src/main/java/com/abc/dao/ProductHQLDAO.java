package com.abc.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import com.abc.util.HibernateUtil;

public class ProductHQLDAO {
	
	
	

	public void hqlUpdate(String productName, String productId) {
		// TODO Auto-generated method stub
		 SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		 Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Query query=session.createQuery("update Product set product_name=:productName  where product_id =:productId");
		query.setString("productName", "productName");
		query.setString("productId", "ProductId");
		int modifications=query.executeUpdate();
		System.out.println("Updated");
		transaction.commit();
	}

}
