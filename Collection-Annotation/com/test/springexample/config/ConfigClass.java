package com.test.springexample.config;

import java.util.ArrayList;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.context.annotation.PropertySource;

import com.test.springexample.beens.Address;

@Configuration
@ComponentScan(basePackages = {"com.test.springexample"})
@PropertySource("classpath:com/test/springexample/properties/input.properties")

public class ConfigClass {
	@Autowired
	@Qualifier(value = "add1")
	private Address add1;
	@Autowired
	@Qualifier(value = "add2")
	Address add2;
	
	
	@Bean(name = "datebeen")
	public Calendar getDate() {
		return Calendar.getInstance();
	}
	
	@Bean(name = "add1")
	public Address add1() {
		Address add1 = new Address();
		return add1;
	}
	@Bean(name = "add2")
	public Address add2() {
		Address add2 = new Address();
		return add2;
	}
	
	@Bean(name = "addresses")
	public ArrayList<Address> getAddresses(){
		ArrayList<Address> addresses = new ArrayList<Address>();
		addresses.add(add1);
		addresses.add(add2);
    	return addresses;
			  
	}
	
}
