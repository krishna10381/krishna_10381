package com.test.springexample.beens;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

@Controller

public class Student {
	@Value("${student.id}")
	private int studentId;
	@Value("${student.name}")
	private String studentName;
	@Value("#{'${student.favSports}'.split(',')}")
	private List<String> favSports;

	@Autowired
	private ArrayList<Address> addresses;
	@Autowired
	private Calendar date;

	public List<String> getFavSports() {
		return favSports;
	}

	public void setFavSports(List<String> favSports) {
		this.favSports = favSports;
	}

	public Calendar getDate() {
		return date;
	}

	public ArrayList<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(ArrayList<Address> addresses) {
		this.addresses = addresses;
	}

	public void setDate(Calendar date) {
		this.date = date;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

}
