package com.test.springexample.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.test.springexample.beens.Student;
import com.test.springexample.config.ConfigClass;

public class MainClass { 
	
	public static void main(String[] args) {
//		ApplicationContext context = 
//				new ClassPathXmlApplicationContext("classpath:com/test/springexample/config/config.xml");
//		
		
		ApplicationContext context = new AnnotationConfigApplicationContext(ConfigClass.class);
		
		Student student = (Student) context.getBean(Student.class);
		System.out.println("id : "+student.getStudentId());
		System.out.println("name : "+student.getStudentName());
		System.out.println("address size" + student.getAddresses().size());
		System.out.println("fav sports: "+ student.getFavSports().get(0)+","+ student.getFavSports().get(1)+","+student.getFavSports().get(2));
//		
		System.out.println("city : " + student.getAddresses().get(1).getCity());
		System.out.println("state : " + student.getAddresses().get(1).getState());
		System.out.println("date : " + student.getDate().getTime());
	} 
}
