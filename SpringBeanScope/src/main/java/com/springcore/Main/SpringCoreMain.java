package com.springcore.Main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.springcore.bean.Address;
import com.springcore.bean.Employee;

/**
 * Hello world!
 *
 */
public class SpringCoreMain {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("com/springcore/config/EmployeeConfig.xml");
		Address address = (Address) context.getBean("address");
		Employee employee = (Employee) context.getBean("employee");
		employee.setAddress(address);
		address.setCity("Vijayawada");
		address.setState("Ap");
		employee.setId(101);
		System.out.println(employee.getId() + "  " + employee.getAddress().getCity());
		Employee employee1 = (Employee) context.getBean("employee");
		System.out.println(employee1.getId());
		
	}
}
