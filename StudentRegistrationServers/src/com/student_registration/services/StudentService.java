package com.student_registration.services;
import java.util.TreeMap;
import com.student_registration.dao.StudentDbconnection;
import com.student_registration.models.StudentMarks;
import com.student_registration.models.StudentModel;

/**
 * this class is handles all the logics of the student 
 * @author IMVIZAG
 *
 */
public class StudentService {
	private StudentDbconnection studentDbconnection;
	/**
	 * this constructor creates the studenDbConnection object
	 */
	public StudentService() {
	     studentDbconnection = new StudentDbconnection();
	}
	
	
	/**
	 * this method regers the new student into the StudentInfo file
	 * 
	 * @param StudentModel Object
	 * @return true if the student is regestered else return false
	 */

	public boolean registerStudent(StudentModel student) {
		
          return studentDbconnection.registerStudent(student);
	}
	
	/**
	 * this method logins the user if the given credentials matches to the database
	 * else return false
	 * 
	 * @param student name and password
	 * @return Studnet Object if loginCredentials matches else return false
	 */
	public StudentModel studentLogin(String userName, String password) {
		return studentDbconnection.studentLogin(userName, password);
	}
	
	/**
	 * This updates the student existing details in the file
	 * 
	 * @param StudentModel Object return true if updation is succesful else it
	 *                     return false
	 */
	public  boolean updateStudent(String studentId,String fieldName,String value ) {
		return studentDbconnection.updateStudentField(studentId, fieldName, value);
	}
	
	public boolean updateFees(String studentId,double fees) {
		int status = studentDbconnection.updateFees(studentId, fees);
		if(status > 0)
			return true;
		else 
			return false;		
	}
	
	
	/**
	 * this method reads the marks from db
	 * @param studentId
	 */
	
	public TreeMap<String, StudentMarks> readMarks(String studentId){
		return studentDbconnection.readMarks(studentId);
	}
	
	/**
	 * THIS METHOD UPDATES THE STUDENT MARKS 
	 */
//    public int updateMarks(String studentId,TreeMap<String, StudentMarks> marksList) {
//    	
//    }
	/**
	 * this method finds the student details if the student found with userId and
	 * seqQue
	 * 
	 * @param String studentId as the key fro the studentsList map and seqQue
	 * @return boolen
	 */
	public  String studentForgotPassWord(String studentId, String securityQuestion) {
		return studentDbconnection.studentForgotPassWord(studentId, securityQuestion);
	}

	/**
	 * this method reset the student password 
	 * @param studentId,new Password,favPlace
	 * @return boolean
	 */
	public boolean resetPassword(String studentId, String newPassword, String favPlace) {
		return  studentDbconnection.resetStudentPassword(newPassword, studentId,favPlace);
	}
	/**
	 * this method delets the student if the student found ,and updates the last
	 * studentId in the UtilityFile
	 * @param studentId
	 */

	// TODO COMPLETE THE DELETE METHOD
	public  boolean deleteStudent(String studentId) {
		return studentDbconnection.deleteStudentById(studentId);
	}
	

	
	/**
	 * this method returns  the new student id 
	 * @throws Exception 
	 */
	public String getNewStudentId() throws Exception {
		return studentDbconnection.getNewStudentId();
	}


	public int updateMarks(String studentId, TreeMap<String, StudentMarks> marksList) {
		return studentDbconnection.updateMarks(marksList, studentId);
	
	}

}
