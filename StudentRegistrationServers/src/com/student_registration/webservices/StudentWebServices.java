package com.student_registration.webservices;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.util.TreeMap;
import com.google.gson.Gson;
import com.student_registration.dao.StudentDbconnection;
import com.student_registration.models.AdminModel;
import com.student_registration.models.StudentLoginModel;
import com.student_registration.models.StudentMarks;
import com.student_registration.models.StudentModel;
import com.student_registration.services.AdminService;

import com.student_registration.services.StudentService;

import com.sun.corba.se.impl.javax.rmi.CORBA.StubDelegateImpl;

@Path("/student")
public class StudentWebServices {
	StudentService studentService = new StudentService();
	StudentDbconnection studentDbconnection = new StudentDbconnection();
	

	@GET
	@Path("/login")
	@Produces("Application/json")

	public String login(@QueryParam("userName") String userName, @QueryParam("password") String password) {
		System.out.println("hai");
		StudentService studentService = new StudentService();
		StudentModel student = studentService.studentLogin(userName, password);
		if (student != null) {
			Gson gson = new Gson();

			String adminJson = gson.toJson(student);
			return adminJson;
		} else {
			return "false";
		}
	}

	/**
	 * This method used to recover the forget password of the student
	 * 
	 * @param studentId
	 * @param newPassword
	 * @param favPlace
	 * @return
	 */
	@GET
	@Path("/forgotPassword")
	public String forgotPassword(@QueryParam("studentId") String studentId,
			@QueryParam("securityQuestion") String securityQuestion) {
		StudentService studentService = new StudentService();
		String student = studentService.studentForgotPassWord(studentId, securityQuestion);
		if (student != null) {
			Gson gson = new Gson();

			String adminJson = gson.toJson(student);
			return adminJson;
		} else {
			return "false";
		}
	}

	/**
	 * This method used to reset the student Password
	 * 
	 * @param studentId
	 * @param newPassword
	 * @param favPlace
	 * @return
	 */
	@GET
	@Path("/resetPassword")
	public String resetPassword(@QueryParam("studentId") String studentId,
			@QueryParam("newPassword") String newPassword, @QueryParam("favPlace") String favPlace) {

		boolean student = studentService.resetPassword(studentId, newPassword, favPlace);
		if (student = false) {
			Gson gson = new Gson();

			String adminJson = gson.toJson(student);
			return adminJson;
		} else {
			return "false";
		}
	}

	/**
	 * This method deletes the Existing student details with the help of student Id
	 * 
	 * @param studentId
	 * @return
	 */
	@GET
	@Path("/deleteStudent")
	public boolean deleteStudent(@QueryParam("studentId") String studentId) {
		Boolean student = studentService.deleteStudent(studentId);
		if (student) {
			return true;

		} else {
			return false;
		}
	}
	
	/**
	 * This method will update the marks of specific student based on student Id
	 * @param studentId
	 * @param marksList
	 * @return
	 */
	@GET
	@Path("/updateMarks")
	public boolean updateMarks(String studentId,TreeMap<String, StudentMarks> marksList) {
		String msg;
		int value = studentService.updateMarks( studentId ,marksList);
		if (value == 1) {
		return true;
		} else {
		return false;
		}

	}
    
    /**
     * This method updates the student Details
     * @param studentId
     * @param fieldName
     * @param value
     * @return
     */
    
	public  boolean updateStudent(@QueryParam("studentId") String studentId, @QueryParam("fieldName")String fieldName,@QueryParam("value")String value ) {
		boolean result =  studentService.updateStudent(studentId, fieldName, value);
		if (result) {
			return true;
		}
		else {
			return false;
		}
	}
    
    /**
     * This method updates the student fees based on studentId
     * @param studentId
     * @param fees
     * @return
     */
	public boolean updateFees(@QueryParam("studentId") String studentId,@QueryParam("fees") double fees) {
		boolean status = studentService.updateFees(studentId, fees);
		if(status)
			return true;
		else 
			return false;		
	}
    
	
    
    
	public boolean registerStudent(StudentModel student) {
		
        return studentDbconnection.registerStudent(student);
	}
    
    
    
    
    
    
    
    
}
