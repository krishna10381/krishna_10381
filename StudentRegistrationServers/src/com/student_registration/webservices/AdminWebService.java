package com.student_registration.webservices;

import javax.websocket.server.PathParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.student_registration.models.AdminModel;
import com.student_registration.services.AdminService;

@Path("/admin")
public class AdminWebService {
	
	@GET
	@Path("/login")
	@Produces("Application/json")

	public String login(@QueryParam("userName") String userName,@QueryParam("password") String password) {
		System.out.println("hai");
		AdminService adminService = new AdminService();
		AdminModel admin = adminService.adminLogin(userName,password);
		if(admin != null) {
			Gson gson = new Gson();
			System.out.println(admin.getAdminUserName());
			String adminJson = gson.toJson(admin);
			return adminJson;
		}
		else {
			return "false";
		}
	}
	
}
