package com.sample.service;

import java.util.List;

import com.sample.bean.Student;

public interface StudentService {
	
	boolean insertStudent( Student student );
	
	
	
	List<Student> fetchAllStudents();
	
	boolean deleteStudent(int studentId);

	Student searchStudent(int studentId);

	int updateStudent(Student student);
}
