package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class SearchStudent
 */
@WebServlet("/SearchStudent")
public class SearchStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * This method search the student by id  and display the student details
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		if (request.getSession().getAttribute("username") == null) {
			out.print("Please Login..");
		}
		else {
		int studentId = 0;
		
	try {
		RequestDispatcher rd = request.getRequestDispatcher("home.html");
		rd.include(request, response);
		studentId = Integer.parseInt(request.getParameter("id"));
		StudentService service = new StudentServiceImpl();
		Student student = service.searchStudent(studentId);
		out.print("<html><body>");
		out.print("<h1><center>STUDENT APPLICATION</center></h1>");
		out.println("<table border='1'>");
		out.println("<tr><th>ID</th><th>Name</th><th>Age</th></tr>");
		out.println("<tr><td>" + student.getId() + "</td>");
		out.println("<td>" + student.getName() + "</td>");
		out.println("<td>" + student.getAge() + "</td></tr>");
		} catch (Exception np) {
			out.print("*****ERROR: ENTER THE VALUE****");
		}
		}
		out.close();
	}

}
