package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/StudentServlet")
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * This method takes the input and insert the data into the student table
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		int studentId = 0;
		String studentName = null;
		int studentAge = 0;
		RequestDispatcher rd = request.getRequestDispatcher("home.html");
		rd.include(request, response);
		if (request.getSession().getAttribute("username") == null) {
			out.print("Please Login..");
		} else {
			try {
				studentId = Integer.parseInt(request.getParameter("id"));
				studentName = request.getParameter("name");
				studentAge = Integer.parseInt(request.getParameter("age"));
				Student student = new Student();
				student.setId(studentId);
				student.setName(studentName);
				student.setAge(studentAge);
				StudentService service = new StudentServiceImpl();
				boolean result = service.insertStudent(student);
				out.print("<html><body>");
				if (result) {
					out.print("<h2>Student Saved</h2>");
				} else {
					out.print("<h2>Something wrong</h2>");
				}
				out.print("</body></html>");
				out.close();
			} catch (Exception np) {
				out.println("****ERROR: ENTER THE VALUES***");
			}
		}
	}
}
