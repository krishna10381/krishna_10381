package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("/HomeServlet")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * This method verifies the credentials of admin
	 * 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter pw = response.getWriter();
		 HttpSession session = request.getSession();
		
		String pwd = null;
		try {
			@SuppressWarnings("unused")
			String name = request.getParameter("name");
			pwd = request.getParameter("password");
				session.setAttribute("username", name);
			response.setContentType("text/html");
			if (pwd.equals("admin123")) {
				RequestDispatcher rd = request.getRequestDispatcher("home.html");
				rd.include(request, response);
			} else {
				pw.println("<h2> Invalid User" + "</h2>");
			}
		} catch (Exception np) {
			pw.print("*****ERROR: ENTER THE VALUES****");
		}
		pw.println("</body></html>");
		pw.close();

	}

}
