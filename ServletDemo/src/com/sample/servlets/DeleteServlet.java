package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class DeleteServlet
 */
@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * This method finds the student data with student id and deletes the following
	 * data
	 * 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		int studentId = 0;
		PrintWriter out = response.getWriter();
		if (request.getSession().getAttribute("username") == null) {
			out.print("Please Login..");
		} else {
		try {
			studentId = Integer.parseInt(request.getParameter("id"));
			StudentService service = new StudentServiceImpl();
			boolean student = service.deleteStudent(studentId);
			if (student == true) {
				
				out.print("Student Deleted");
			} else {
				out.println("<h1><center>STUDENT APPLICATION</center></h1>");
				out.println("Student doesnt exist");
			}

		} catch (Exception e) {
			out.print("*******ERROR: ENTER ID TO DELETE THE DATA******");
		} 
		}
	}
}
