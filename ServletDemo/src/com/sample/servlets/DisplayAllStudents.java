package com.sample.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class DisplayAllStudents
 */
@WebServlet("/DisplayAllStudents")
public class DisplayAllStudents extends HttpServlet {
	private static final long serialVersionUID = 1L;
	/**
	 * This method prints the all data of students that is found in student table
	 * @param request,response
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		if (request.getSession().getAttribute("username") == null) {
			out.print("Please Login..");
		} else {
		StudentService service = new StudentServiceImpl();
		List<Student> studentList = service.fetchAllStudents();
		RequestDispatcher rd = request.getRequestDispatcher("home.html");
							rd.include(request,response);
		Iterator<Student> i = studentList.iterator();

		
		out.print("<html><body>");
		
		out.println("<table border='1'>");
		out.println("<tr><th>ID</th><th>Name</th><th>Age</th></tr>");
		while (i.hasNext()) {
			Student student = i.next();
			out.println("<tr><td>" + student.getId() + "</td>");
			out.println("<td>" + student.getName() + "</td>");
			out.println("<td>" + student.getAge() + "</td></tr>");
		}
		out.println("</table>");
		out.print("</body></html>");
		out.close();
		}
	}

}
