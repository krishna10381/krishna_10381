package com.sample.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.sample.bean.Student;
import com.sample.util.DBUtil;

public class StudentDAOImpl implements StudentDAO {

	/**
	 * This method inserts the student data in student table
	 * 
	 * @param student
	 * @return boolean
	 */
	@Override
	public boolean createStudent(Student student) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "insert into student_tbl values( ?, ?, ?)";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, student.getId());
			ps.setString(2, student.getName());
			ps.setInt(3, student.getAge());
			int rowsEffected = ps.executeUpdate();
			if (rowsEffected == 1) {
				result = true;
			}

		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	/**
	 * This method searches the student data based on student id
	 * 
	 * @param id
	 * @return student
	 */
	@Override
	public Student searchById(int id) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Student st = null;
		String sql = "select * from student_tbl where id = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				st = new Student();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));

			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return st;
	}

	/**
	 * This method fetches the all student data from student table
	 * 
	 * @return List<Student>
	 */
	@Override
	public List<Student> getAllStudents() {

		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Student st = null;
		List<Student> studentList = new ArrayList<Student>();
		String sql = "select * from student_tbl ";
		try {
			con = DBUtil.getCon();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				st = new Student();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				studentList.add(st);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return studentList;
	}

	/**
	 * This method delete the student data in student table with the help of student
	 * id
	 * 
	 * @param id
	 * @return boolean
	 */

	@Override
	public boolean deleteStudent(int id) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		Student st = null;
		String sql = "delete from student_tbl where id = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			int rowsEffected = ps.executeUpdate();
			if (rowsEffected == 1) {
				result = true;
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;

	}

	/**
	 * This method Update the student data in student table
	 * 
	 * @param student
	 * @return integer
	 */
	@Override
	public int updateStudent(Student student) {
		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "update  student_tbl set age = ? , name = ?  where studentId = ? ";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, student.getId());
			ps.setString(2, student.getName());
			ps.setInt(3, student.getId());
			int rowsEffected = ps.executeUpdate();
			if (rowsEffected == 1) {
				result = true;
			}

		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return 1;
	}
}
