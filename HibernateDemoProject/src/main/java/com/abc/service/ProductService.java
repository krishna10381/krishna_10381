package com.abc.service;

import com.abc.dao.ProductDAO;
import com.abc.entity.Product;

public class ProductService {
	
	ProductDAO productDAO;
	public ProductService() {
	productDAO = new ProductDAO();
}
	public boolean createProduct(Product product) {
		return productDAO.create(product);
		
	}
}
