package com.properties.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.properties.bean.PropertiesBean;

public class PropertiesMain {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("com/properties/config/config.xml");
		PropertiesBean bean = (PropertiesBean) context.getBean("propertiesBean");
		System.out.println(bean.getFirstName());
		System.out.println(bean.getLastName());
		

	}

}
