package com.abc.main;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.abc.dao.ProductDAO;
import com.abc.dao.ProductHQLDAO;
import com.abc.entity.Customer;
import com.abc.entity.Product;
import com.abc.service.ProductService;
import com.abc.util.HibernateUtil;

public class ProductMain {

	public static void main(String[] args) {
//		// code to insert the new record
//		Product product = new Product();
//		product.setProduct_id("P112");
//		product.setProduct_name("OnePlus6T");
//		product.setProduct_price(900000);
//		product.setCategory("Mobile");
//		ProductService productService = new ProductService();
//		productService.createProduct(product);
//
//		// Code to update existing details
		Product product1 = new Product();
		product1.setProduct_id("P109");
//		product1.setProduct_name("SAMSUNG");
//		product1.setProduct_price(800000);
//		product1.setCategory("SMART WATCH");
//		boolean result = productService.updateProduct(product1);
//		System.out.println(result);
//
//		// code to update record using hql quries
		Product product2 = new Product();
		product2.setProduct_id("p110");
//		product2.setProduct_name("ORANGE");
//		product2.setProduct_price(550000);
//		product2.setCategory("LED_SCREEN");
//		boolean saveOrUpdate = productService.saveOrUpdate(product2);
//		ProductHQLDAO hqlDao = new ProductHQLDAO();
//		hqlDao.hqlUpdate("BENZ", "P103");
//		
//		
//		//calling methods to retrieve records from db
//		ProductDAO.getAllData();
//		ProductDAO.getSelectedData();

		
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		Customer customer1 = new Customer();
		customer1.setCustomerId("C101");
		customer1.setCustomerName("Mani");
		
		Customer customer2 = new Customer();
		customer2.setCustomerId("C102");
		customer2.setCustomerName("Honey");
		
		Set <Product> customerOneProducts = new HashSet<Product>();
		customerOneProducts.add(product2);
		customerOneProducts.add(product1);
		
		
		Set <Product> customerTwoProducts = new HashSet<Product>();
		customerTwoProducts.add(product1);
		
		
		customer1.setProduct(customerOneProducts);
		customer2.setProduct(customerTwoProducts);

		session.save(customer1);
		session.save(customer2);
		session.getTransaction().commit();
		
 		
	}

}
