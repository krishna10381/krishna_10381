package com.abc.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Customer {

	
	@Id
	@Column(name = "customerId")
	private String customerId;
	
	@Column(name = "customerName")
	private String customerName;
	
	/**
	 * @return the customerId
	 */
	public String getCustomerId() {
		return customerId;
	}

	/**
	 * @param customerId the customerId to set
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the product
	 */
	public Set<Product> getProduct() {
		return product;
	}
                                             

	/**
	 * @param product the product to set
	 */
	public void setProduct(Set<Product> product) {
		this.product = product;
	}

	@OneToMany
	private Set<Product> product;
	
}
