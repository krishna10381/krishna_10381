package com.abc.service;

import com.abc.dao.ProductDAO;
import com.abc.entity.Product;

public class ProductService {
	
	ProductDAO productDAO;
	public ProductService() {
	productDAO = new ProductDAO();
}
	public boolean createProduct(Product product) {
		return productDAO.create(product);
		
	}
	public boolean updateProduct(Product product1) {
		return productDAO.update(product1);
		
	}
	public boolean saveOrUpdate(Product product2) {
		// TODO Auto-generated method stub
		return productDAO.saveOrUpdate(product2);
	}
}
