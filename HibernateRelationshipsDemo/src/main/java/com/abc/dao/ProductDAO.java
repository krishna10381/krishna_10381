package com.abc.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

import com.abc.entity.Product;
import com.abc.util.HibernateUtil;

public class ProductDAO {
	SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	Session session = sessionFactory.openSession();
	Transaction transaction = session.beginTransaction();

	/**
	 * save method to insert the record in the database
	 * 
	 * @param product
	 * @return
	 */
	public boolean create(Product product) {

		session.save(product);
		transaction.commit();
		System.out.println("Inserted");
		return true;

	}

	/**
	 * This method updates the existing value that is already presented in the
	 * database
	 * 
	 * @param product
	 * @return
	 */
	public boolean update(Product product) {
		session.update(product);
		transaction.commit();
		return true;
	}

	/**
	 * This method updates the existing value that is already presented in the
	 * database if it is not presented it will insert the values in the database
	 * 
	 * 
	 * @param product
	 * @return
	 */

	public boolean saveOrUpdate(Product product2) {
		session.saveOrUpdate(product2);
		transaction.commit();
		return true;
	}

	/**
	 * This method fetch the all data.
	 * 
	 */

	public static void getAllData() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		// Criteria criteria = session.createCriteria(Product.class);
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Product> criteria = builder.createQuery(Product.class);
		Root<Product> root = criteria.from(Product.class);
		criteria.select(root);
		Query<Product> query = session.createQuery(criteria);
		List<Product> list = query.list();
		for (Product product : list) {
			System.out.println(product.getProduct_id() + "      " + product.getProduct_name() + "       "
					+ product.getProduct_price() + "         " + product.getCategory());
		}
	}

	/**
	 * This method fetches the selected data of product
	 */

	public static void getSelectedData() {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Criteria crit = session.createCriteria(Product.class);
		crit.add(Restrictions.eq("category", "Mobile"));
		@SuppressWarnings("rawtypes")
		List<Product> result = crit.list();
		for (Product product : result) {
			System.out.println(product.getProduct_id() + "  " + product.getProduct_name());
		}
	}

	/**
	 * Loading object using session load method "Early Loding"
	 */

//	public static void loadDemo() {
//		Product product = session.load(Product.class, "P102");
//		System.out.println(product.getProduct_id() + "     " + product.getProduct_name() + "    "
//				+ product.getProduct_price() + "    " + product.getCategory());
//	}
//
//	/**
//	 * This method works as the same as load but If object is not fount in the
//	 * database it returns null instead of exception. load method retuns Exception
//	 * if object is not found
//	 */
//	public static void getDemo() {
//		Product product = session.get(Product.class, "P102");
//		System.out.println(product.getProduct_id() + "     " + product.getProduct_name() + "    "
//				+ product.getProduct_price() + "    " + product.getCategory());
//	}
	
	
	
	
	

	
	
}
