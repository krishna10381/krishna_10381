package com.sample.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class HomeServlet
 */
@WebServlet("/HomeServlet")
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * This method verifies the credentials of admin
	 * 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		RequestDispatcher red = request.getRequestDispatcher("HomeErrors.jsp");
		String pwd = null;
		try {
			String name = request.getParameter("name");
			pwd = request.getParameter("password");
			session.setAttribute("username", name);
			response.setContentType("text/html");
			if (pwd.equals("admin123")) {
				RequestDispatcher rd = request.getRequestDispatcher("StudentRegistration.jsp");
				request.setAttribute("msg", "Welcome...");
				rd.forward(request, response);
			} else {
				
				request.setAttribute("msg", "Password Incorrect..");
				red.forward(request, response);
			}
		} catch (Exception np) {
			request.setAttribute("msg", "Enter UserName and Password");
			red.forward(request, response);
		}

	}

}
