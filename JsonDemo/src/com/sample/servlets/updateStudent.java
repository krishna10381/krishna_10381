package com.sample.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class updateStudent
 */
@WebServlet("/updateStudent")
public class updateStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * This method finds the student details and update the student details based on
	 * student id
	 * 
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int studentId = 0;
		
		if (request.getSession().getAttribute("username") == null) {
			response.sendRedirect("ErrorLogin.jsp");
		} else {
			studentId = Integer.parseInt(request.getParameter("id"));
			System.out.println(studentId);
			StudentService service = new StudentServiceImpl();
			Student student = service.searchStudent(studentId);

			if (student != null) {
				ServletContext sc = getServletContext();
				RequestDispatcher rd = request.getRequestDispatcher("home.html");
				rd.include(request, response);
				sc.getRequestDispatcher("/FillDetails.html").include(request, response);

				@SuppressWarnings("unused")
				String name = request.getParameter("name");
				int age = Integer.parseInt(request.getParameter("age"));
				Student student1 = new Student();
				student1.setId(studentId);
				student1.setName("name");
				student1.setAge(age);
				int result = service.updateStudent(student1);

				if (result == 1) {

					response.sendRedirect("StudentRegistration.jsp");
				} else {
					response.sendRedirect("TryAgain.jsp");
				}
			}
		}

	}
}
