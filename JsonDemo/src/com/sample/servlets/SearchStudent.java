package com.sample.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.sample.bean.Student;
import com.sample.service.StudentService;
import com.sample.service.StudentServiceImpl;

/**
 * Servlet implementation class SearchStudent
 */
@WebServlet("/SearchStudent")
public class SearchStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * This method search the student by id and display the student details
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("StudentRegistration.jsp");
		if (request.getSession().getAttribute("username") == null) {
			request.setAttribute("msg", "Please Login");
			rd.forward(request, response);
		} else {
			int studentId = 0;
			try {
				studentId = Integer.parseInt(request.getParameter("id"));
				StudentService service = new StudentServiceImpl();
				Student student = service.searchStudent(studentId);
				if (student != null) {
					RequestDispatcher red = request.getRequestDispatcher("SearchResult.jsp");
					request.setAttribute("student", student);
					red.forward(request, response);
				} else {
					request.setAttribute("msg", "Student Not Found");
					rd.forward(request, response);
				}
			} catch (Exception np) {
				request.setAttribute("msg", "Please Enter the values");
				rd.forward(request, response);
			}
		}

	}

}
