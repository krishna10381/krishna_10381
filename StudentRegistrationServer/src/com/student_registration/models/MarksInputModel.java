package com.student_registration.models;

import java.util.TreeMap;

public class MarksInputModel {
	private String studentId;
	private TreeMap<String, StudentMarks> studentMarksList;
	/**
	 * @return the studentId
	 */
	public String getStudentId() {
		return studentId;
	}
	/**
	 * @param studentId the studentId to set
	 */
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	/**
	 * @return the studentMarksList
	 */
	public TreeMap<String, StudentMarks> getStudentMarksList() {
		return studentMarksList;
	}
	/**
	 * @param studentMarksList the studentMarksList to set
	 */
	public void setStudentMarksList(TreeMap<String, StudentMarks> studentMarksList) {
		this.studentMarksList = studentMarksList;
	}
	

}
