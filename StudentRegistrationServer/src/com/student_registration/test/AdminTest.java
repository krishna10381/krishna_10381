package com.student_registration.test;
/**
 * this class tests admin all operations 
 * @author IMVIZAG
 *
 */

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.student_registration.models.AdminModel;
import com.student_registration.services.AdminService;

public class AdminTest {
	AdminService adminService = null;

	@Before
	@Test
	public void setUp() {
		System.out.println("before");
		adminService = new AdminService();
	}

	@After
	@Test
	public void tearDown() {
		System.out.println("after");
		adminService = null;
	}

	/**
	* 
	*/
	@Test
	public void adminLoginPositiveTest() {

		System.out.println("method");
		assertNotNull(adminService.adminLogin("mani", "Mani@123"));
	}

	@Test
	public void adminUsernameNegativeTest() {
		assertNull(adminService.adminLogin("manisxs", "Mani@123"));
	}

	@Test
	public void adminPasswordNegativeTest() {
		assertNull(adminService.adminLogin("mani", "Mani@123456"));
	}

	@Test
	public void adminUsernamePasswordNegativeTest() {
		assertNull(adminService.adminLogin("vmanisdsds", "Mani@1234"));
	}

	@Test
	public void adminRegisterPositiveTest() {
		AdminModel admin = new AdminModel();
		admin.setUniqueId("GROUP-B");
		admin.setAdminUserName("rakesh");
		admin.setAdminPassword("Rakesh@123");
		// assertNotNull(adminService.signUpAdmin(admin));
	}

	@Test
	public void adminRegisterNegativeTest() {

	}

	@Test
	public void forgotPasswordPositiveTest() {
		assertNotNull(adminService.adminForgotPassWord("mani", "myVillage"));
	}

	@Test
	public void forgotPasswordNegativeTest() {
		assertNull(adminService.adminForgotPassWord("mani", "myVillageBSBSJDBJS"));
	}

	@Test
	public void resetPassWordPositiveTest() {
		assertTrue(adminService.resetAdminPassword("Mani@123", 1));
	}

	@Test
	public void resetPassWordNegativeTest() {
		assertFalse(adminService.resetAdminPassword("Mani@123", 10101));
	}

}
