package com.student_registration.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.student_registration.models.AdminModel;
import com.student_registration.utilities.DataBaseConnector;
import com.student_registration.utilities.UtilityDao;

/**
 * this class is created for doing all the CURD operations into the files
 * 
 * @author Group B
 *
 */
public class AdminDbConnection {
	public static final String UNIQKEY = "GROUP-B";
	UtilityDao utilityDao = new UtilityDao();

	public void signUpAdmin(AdminModel admin) throws Exception {
		Connection con = DataBaseConnector.getConnection();
		if (admin.getUniqueId().equals(UNIQKEY)) {
			if(con == null) {
				throw new Exception("sorry some thing went wrong");
			}
			PreparedStatement ps = con.prepareStatement("select * from admin_table where userName = ?");
			ps.setString(1, admin.getAdminUserName());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				con.close();
				throw new Exception("Already same user exists with this username..");
			}

			ps = con.prepareStatement("insert into admin_table(username,password,securityQue) values(?,?,?)");
			ps.setString(1, admin.getAdminUserName());
			ps.setString(2, admin.getAdminPassword());
			ps.setString(3, admin.getAdminSecurityQue());
			int result = ps.executeUpdate();
			if (result == 0) {
					con.close();
					throw new Exception("sorry some thing went wrong");
			}
	   }else {
			con.close();
			throw new Exception("Entered uniqueId is not matched");
		}
	}

	/**
	 * this method checks the admin details,if found return the boolen value
	 * 
	 * @param adminInfo
	 * @return true if the admin is Valid admin else return false
	 */

	public AdminModel adminLogin(String username, String password) {
		AdminModel admin = null;
		Connection con = DataBaseConnector.getConnection();
		if(con == null)
			return null;
		try {
			PreparedStatement ps = con
					.prepareStatement("select * from admin_table where userName = ? and password = ?");
			ps.setString(1, username);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				// filling the admin model
				admin = new AdminModel();
				admin.setAdminUserName(rs.getString("userName"));
				admin.setAdminPassword(rs.getString("password"));
				admin.setAdminSecurityQue(rs.getString("securityQue"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return admin;

	}

	/**
	 * this method searching the admin by the username and security code returns the
	 * key of the admin
	 * 
	 * @param adminForgotDetails
	 * @return admin index in treeMap of admins
	 */

	public String adminForgotPassWord(String userName, String securityQue) {

		Connection con = DataBaseConnector.getConnection();
		try {
			if(con == null)
				return null;
			PreparedStatement ps = con
					.prepareStatement("select * from admin_table where userName = ? and securityQue = ?");
			ps.setString(1, userName);
			ps.setString(2, securityQue);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return String.valueOf((rs.getInt(1)));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * this method resets the admin password
	 * 
	 * @param object index or key of map ,new password details
	 * 
	 */
	public boolean resetAdminPassword(String newPassWord, int searchKey) {
		Connection con = DataBaseConnector.getConnection();
		try {
			if(con == null)
				return false;
			PreparedStatement ps = con.prepareStatement("update admin_table set password = ? where empId = ?");
			ps.setString(1, newPassWord);
			ps.setInt(2, searchKey);
			int res = ps.executeUpdate();
			if (res > 0)
				return true;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;

	}
	
	
	


}
