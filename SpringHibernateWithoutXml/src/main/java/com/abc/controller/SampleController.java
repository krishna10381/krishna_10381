package com.abc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SampleController {
	
	@GetMapping("/hello")
	public String sayHello() {
		return "sample";
	}
	
	@GetMapping("/greets")
	public ModelAndView greeting() {
		String myData ="Spring mvc Greetings you";
		ModelAndView mav= new ModelAndView("result","modalData",myData);
		return mav;
	}
	
	

}
