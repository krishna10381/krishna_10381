package com.demo.services;

import java.util.List;

import com.demo.entity.User;

public interface UserService {
	 void add(User user);
	    List<User> listUsers();

}
