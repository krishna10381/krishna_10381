package com.demo.dao;

import java.util.List;

import com.demo.entity.User;

public interface UserDao {

	void add(User user);

	List<User> listUsers();

}
