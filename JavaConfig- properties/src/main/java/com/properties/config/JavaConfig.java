package com.properties.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import com.properties.bean.PropertiesBean;

@Configuration
@ComponentScan(basePackages = {"com.properties"})
@PropertySource("com/properties/values/values.properties")
public class JavaConfig {


	
}
