package com.properties.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import com.properties.bean.PropertiesBean;

@Configuration
@PropertySource("com/properties/values/values.properties")
public class JavaConfig {

	@Autowired
	Environment env;
	
	@Bean(name = "propertiesbean")
	public PropertiesBean getBeanValues() {
		PropertiesBean propertiesbean = new PropertiesBean();
		propertiesbean.setFirstName(env.getProperty("values.firstName"));
		propertiesbean.setLastName(env.getProperty("values.lastName"));
		return propertiesbean;
		
	}
	
	
}
