package com.properties.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.properties.bean.PropertiesBean;
import com.properties.config.JavaConfig;

public class PropertiesMain {

	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(JavaConfig.class);
		PropertiesBean bean = (PropertiesBean) context.getBean("propertiesbean");
		System.out.println(bean.toString());
	
		

	}

}
