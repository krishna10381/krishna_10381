package com.sample.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sample.bean.Address;
import com.sample.bean.Student;
import com.sample.util.DBUtil;

/**
 * This class is used for inserting, updating, displaying and deleting the
 * Student information
 * 
 * @author IMVIZAG
 *
 */

public class StudentDAOImpl implements StudentDAO {

	/**
	 * This method is used to insert the student data and returns the boolean result
	 * "true" if the student data is inserted
	 */

	@Override
	public boolean createStudent(Student student) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;
		String sql = "insert into student_tbl values( ?, ?, ?, ?, ?, ?)";
		try {

			// establishing the connection
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);			
			ps.setInt(1, student.getId());
			ps.setString(2, student.getName());
			ps.setInt(3, student.getAge());
			ps.setString(4, student.getAddress().getCity());
			ps.setString(5, student.getAddress().getState());
			ps.setString(6, student.getAddress().getPincode());
			int rowsEffected = ps.executeUpdate();
			if (rowsEffected == 1) {
				result = true;
			}

		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		// returns true if the data is inserted and false if data is not inserted to the
		// service package
		return result;
	}

	/**
	 * this method is used for searching the student data based on the student id
	 */

	@Override
	public Student searchById(int id) {

		// establishing the connection
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Student st = null;
		String sql = "select * from student_tbl where id = ?";
		try {
			con = DBUtil.getCon();
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			while (rs.next()) {
				Address address = new Address();
				address.setCity(rs.getString(4));
				address.setState(rs.getString(5));
				address.setPincode(rs.getString(6));
				st = new Student();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				st.setAddress(address);
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		// returns the student object to the service package
		return st;
	}

	/**
	 * this method is used for displaying all the students in the database
	 */

	@Override
	public List<Student> getAllStudents() {

		// establishing the connection
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		Student st = null;
		List<Student> studentList = new ArrayList<Student>();
		String sql = "select * from student_tbl ";
		try {
			con = DBUtil.getCon();
			stmt = con.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				Address address = new Address();
				address.setCity(rs.getString(4));
				address.setState(rs.getString(5));
				address.setPincode(rs.getString(6));
				st = new Student();
				st.setId(rs.getInt(1));
				st.setName(rs.getString(2));
				st.setAge(rs.getInt(3));
				st.setAddress(address);
				studentList.add(st);
			}
		} catch (SQLException e) {

		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		// returns the list to the service package
		return studentList;
	}

	/**
	 * this method is used for deleting the student data from the database
	 */

	@Override
	public boolean deleteStudent(int id) {

		// establishing the connection
		Connection con = null;
		boolean result = false;
		try {
			con = DBUtil.getCon();
			String sql = "delete from student_tbl where id = ? ";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			int res = ps.executeUpdate();
			if (res > 0) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		// returns true if the data is deleted and false if data is not deleted to the
		// service package
		return result;
	}

	/**
	 * this method is used for updating the student data from the database
	 */
	
	@Override
	public boolean updateStudent(Student student) {

		// establishing the connection
		Connection con = null;
		PreparedStatement ps = null;

		int id = student.getId();
		String name = student.getName();
		int age = student.getAge();
		boolean result = false;
		String sql = "update student_tbl SET name = ?, age = ? where id = ?";
		try {
			con = DBUtil.getCon();
			System.out.println("");
			ps = con.prepareStatement(sql);
			ps.setString(1, name);
			ps.setInt(2, age);
			ps.setInt(3, id);
			int rowsEffected = ps.executeUpdate();
			if (rowsEffected == 1) {
				result = true;
			}
		} catch (SQLException e) {
			System.out.println(e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		// returns true if the data is updated and false if data is not updated to the
		// service package
		return result;
	}

}