package com.student_registration.models;

import java.io.Serializable;

public class AdminModel implements Serializable {
	private static final long serialVersionUID = 6529685098267757690L;
	private String uniqueId;
	private String adminUserName;
	private String adminPassword;
	private String adminReEnterPassword;
	private String adminSecurityQue;

	public String getAdminSecurityQue() {
		return adminSecurityQue;
	}

	public void setAdminSecurityQue(String adminSecurityQue) {
		this.adminSecurityQue = adminSecurityQue;
	}

	public String getAdminReEnterPassword() {
		return adminReEnterPassword;
	}

	public void setAdminReEnterPassword(String adminReEnterPassword) {
		this.adminReEnterPassword = adminReEnterPassword;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public String getAdminUserName() {
		return adminUserName;
	}

	public void setAdminUserName(String adminUserName) {
		this.adminUserName = adminUserName;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}
}
