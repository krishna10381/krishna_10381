package com.student_registration.utilities;

import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.net.URI;  
import javax.ws.rs.client.Client;  
import javax.ws.rs.client.ClientBuilder;  
import javax.ws.rs.client.WebTarget;  
import javax.ws.rs.core.MediaType;  
import javax.ws.rs.core.UriBuilder;  
import org.glassfish.jersey.client.ClientConfig;

import com.mysql.jdbc.StandardSocketFactory;
import com.student_registration.controllers.AdminPage;
import com.student_registration.controllers.HomePage;
import com.student_registration.controllers.StudentPage;
import com.student_registration.dao.StudentDbconnection;
import com.student_registration.models.AdminModel;
import com.student_registration.models.StudentMarks;
import com.student_registration.models.StudentModel;
import com.student_registration.services.AdminService;
import com.student_registration.services.SearchStudent;
import com.student_registration.services.StudentService;
import com.student_registration.validations.AdminValidations;

/**
 * this class provides the common methods which are used for the admin and student
 * @author IMVIZAG
 *
 */
public class CommonClass {
	AdminValidations adminValidation;
	StudentService studentService;
	AdminService adminService;
	
	public CommonClass() {
		adminValidation = new AdminValidations();
	    studentService = new StudentService();
	    adminService = new AdminService();
	}
	/**
	 * this method shows the student fees details by data and time wise 
	 * @param String studentId
	 *
	 * 
	 */
	public void viewFeeDetails(String studentId){
		UtilityDao utilityDao	= new UtilityDao();
	   TreeMap<String,Double> feeDetails =  utilityDao.getFeesDetails(studentId);
	   StudentModel student = utilityDao.fetchStudentDetails(studentId);
	   double totalPaidFee = 0;
	   if(feeDetails.isEmpty()) {
		   System.out.println("FEES NOT UPDATED");
	   } else {
	   Set<String> keys = feeDetails.keySet(); 
	   System.out.println("               Fee Details                   ");
		System.out.println("==============================================================");
		System.out.println("fee_Paid_Date       		Time   	  	       amount");
		System.out.println("==============================================================");
		for(String key : keys) {
			double fee = feeDetails.get(key);
			totalPaidFee += fee;
		String[] dateTime = key.split(" ");
			System.out.println(dateTime[0] +"            "+dateTime[1]+"               "+ fee);
		}
		
		System.out.println("----------------------------------------------------------------");
		System.out.println("total Paid:==================>>>   "+totalPaidFee);
		System.out.println("----------------------------------------------------------------");
		System.out.println("Remaining Due:"+"=============>>>  "+student.getDue());
		System.out.println("-------------XXXXXXXXXXXXXXXX-----------------------------------");
		System.out.println();
		System.out.println();
		
	 }
	}
	
	/**
	 * this method shows the Admin Login form
	 * 
	 * @param loginAttempts,scanner
	 */
	public void showLoginForm( int loginAttempts, String context) {
		boolean callAgain = true;
		Scanner scan = new Scanner(System.in);
		System.out.println("                            " + context + " Login Page \n                   --------------------------------------------");
		
		outerWhileLoop: while (callAgain) {

			if (context.equals("Admin")) {
				System.out.print("Enter " + context + " UserName:");
			}
			if (context.equals("Student")) {
				System.out.print("Enter " + context + " ID:");
			}
			String userName = scan.next();
			boolean isValidUserName = false;
			
			while (!isValidUserName) {
				isValidUserName = adminValidation.adminUserNameValidation(userName);
				if (isValidUserName == true) {
					break;
				}
				System.out.println("Invalid Username..Please Enter Valid Name!!!");
				if (context.equals("Admin")) {
					System.out.print("Enter " + context + " UserName:");
				} else {
					System.out.print("Enter " + context + " ID:");
				}
				userName = scan.next();
			}

			// Askin Admin to enter his password ;
			System.out.print("Enter Password:");
			String password = scan.next();
			boolean isValidPassword = false;

			while (!isValidPassword) {
				isValidPassword = AdminValidations.adminPasswordValidate(password);
				if (isValidPassword == true) {
					break;
				}
				System.out.println("Invalid Password!!!Password should contain atleast 8 characters");
				System.out.print("Enter Password:");
				password = scan.next();
			}
			
            //admin Login check and calling the admin operations 
			if (context.equals("Admin")) {
				//AdminModel admin = adminService.adminLogin(userName, password);
				
                 WebTarget target = getWebTarget();
				    //Now printing the server code of different media type  
				 String adminJson = target.path("admin/login").queryParam("userName",userName).queryParam("password", password).request().get(String.class);
				 
				 System.out.println(adminJson);
				 AdminModel admin = JsonDeserializer.getObject(adminJson,AdminModel.class);
				    
				 if (admin != null) {
					System.out.println("Login success ");
					new AdminPage().adminsOperations(admin);
					callAgain = false;
					break;
				} else {
					System.out.println("Invalid UserName/Password");
					System.out.println("____________________________________");
					loginAttempts++;
					if (loginAttempts == 3) {
						System.out.println("you have entered the incorect password three times ,Do you want to reset the password ? (type yes/no)");
						while(true){
							String conform = scan.next();
							if(conform.equalsIgnoreCase("yes")){
							    showForgotPassword( context);
							    break outerWhileLoop;
							}else if(conform.equalsIgnoreCase("no")) {
								new HomePage().showHomePage();
								break outerWhileLoop;
							}
							else {
								System.out.println("please enter yes/no only...");
							}
							
						}
					}
				}

			} else {//Student login check and calling the student Oparations 
				
                WebTarget target = getWebTarget();
				    //Now printing the server code of different media type  
				 String adminJson = target.path("student/login").queryParam("userName",userName).queryParam("password", password).request().get(String.class);

				 StudentModel admin = JsonDeserializer.getObject(adminJson,StudentModel.class);
				if (admin != null) {
					new StudentPage().studentOperations(admin);
					callAgain = false;

				} else {
					System.out.println("Invalid UserName/Password");
					System.out.println("____________________________________");
					loginAttempts++;
					if (loginAttempts == 3) {
						System.out.println("you have entered the incorect password three times ,Do you want to reset the password ? (type yes/no)");
						
						while(true){
							String conform = scan.next();
							if(conform.equalsIgnoreCase("yes")){
							    showForgotPassword(context);
							    break outerWhileLoop;
							}
							else if(conform.equalsIgnoreCase("no") ){
								new HomePage().showHomePage();
								break outerWhileLoop;
							}
							else {
								System.out.println("please enter yes/no only...");
							}
						}
				    }

				}
			}

		}
		scan.close();
	}

	/**
	 * this method shows the forgot password screen for both admin and student
	 * 
	 * @param scan
	 * @param context
	 * @return and returns true if the user is valid
	 */

	public void showForgotPassword( String context) {
		Scanner scan = new Scanner(System.in);
		int attempts = 0;
		whileLoop: while (true) {
			if (context.equals("Admin")) {
				System.out.print("Enter " + context + " UserName:");
			} else {
				System.out.print("Enter " + context + " ID:");
			}
			String userName = scan.next();
			// Askin Admin to enter his password ;
			System.out.print("Enter your favorite place");
			String securityQue = scan.next();
         
			String foundAdminOrStudentKey;
			//if the admin or student found in database with username and favorite place it returns the key of that object
			if (context.equals("Admin")) {
				
		                  
						    //Now printing the server code of different media type  
				foundAdminOrStudentKey  = getWebTarget().path("admin/forgotPassword").queryParam("userName",userName).queryParam("securityQue", securityQue).request().get(String.class);
				System.out.println(foundAdminOrStudentKey);		
						
						
						
						//adminService.adminForgotPassWord(userName,securityQue);
			} else {
				
				foundAdminOrStudentKey = getWebTarget().path("student/forgotPassword").queryParam("userName",userName).queryParam("securityQue", securityQue).request().get(String.class);
						
			}

			if (foundAdminOrStudentKey != null) {
				System.out.println("________________________________");
				// showing the option to change the password
				if (resetPassword(context, foundAdminOrStudentKey)) {
					System.out.println("password changed Successfully");
					System.out.println();
					showLoginForm( 0, context);

					break whileLoop;

				} else {
					// again showing the same page
					System.out.println("________________________________");
					//showForgotPassword(context);
				}
			} else {
				attempts++;
				
				if (attempts == 3) {
					System.out.println(
							"You have entered 3 times wrong info ,your account locked for few minutes,please try after some time  ");
					new HomePage().showHomePage();
					break whileLoop;
				}
				else {
					System.out.println("Given information is not matched ..you have  "+ (3 - attempts) + " attempts remain ");
				}
			}
		}
		scan.close();

	}

	/**
	 * this method showing the reset the password page
	 * @param scan
	 * @param context
	 * @param searchKey
	 * @return boolean 
	 */
	public boolean resetPassword(String context, String searchKey) {
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		String password;
		do {
			System.out.print("Enter new password");
			password = scan.next();
		} while (!AdminValidations.adminPasswordValidate(password));

		System.out.print("confirm password ");
		String conformPass = scan.next();
		while (!(password.equals(conformPass))) {
			System.out.println("Please Enter Correct Password");
			System.out.print("Re-Enter Password:");
			conformPass = scan.next();
		}
		String favPlace = "";
		if (context.equalsIgnoreCase("Student")) {
			System.out.println("What is your favorite place?");
			favPlace = scan.next();

		}

		if (context.equals("Admin")) {
			int key = Integer.parseInt(searchKey);
			
			 String result = getWebTarget().path("admin/resetPassword").queryParam("newPassword",password).queryParam("key", searchKey).request().get(String.class);
			
			 Boolean resetStatus = Boolean.parseBoolean(result);
			
			return resetStatus;
		} else {
			boolean resetStatus = studentService.resetPassword(searchKey, password, favPlace);
			
			return resetStatus;
		}

	}
	
	/**
	 * this method serches the student with student Id and show the student marks
	 * 
	 * @param studentId
	 */
	public void showMarks(String studentId) {
		SearchStudent searchStudent = new SearchStudent();
		
		StudentModel student = searchStudent.searchById(studentId);
		if(student == null) {
			System.out.println("PRINTED");
		}
		if (student != null) {
			if ((student.getPusrsuingYear()) < 1) {
				System.out.println("You are the first year student only ,no marks updates for you ");
				return;
			}
			if (student.getPusrsuingYear() == 2 && student.getQualification().equalsIgnoreCase("diploma")) {
				System.out.println("You are the diploma student ,no marks for first year ");
				return;
			}
			TreeMap<String, StudentMarks> studentMarksList = new StudentDbconnection().readMarks(student.getStudentId());
			if (!studentMarksList.isEmpty()) {
				System.out.println(
						"============================================================================================================");
				System.out.println(
						"SemisterName    subject1    subject2  subject3  subject4   subject5   subject6    percentage    backlogs ");
				System.out.println(
						"============================================================================================================");
				Set<Map.Entry<String, StudentMarks>> entrySet = studentMarksList.entrySet();
				for (Map.Entry<String, StudentMarks> entry : entrySet) {
					StudentMarks marks = entry.getValue();
					double average = (marks.getSubject1() + marks.getSubject2() + marks.getSubject3()
							+ marks.getSubject4() + marks.getSubject5() + marks.getSubject6()) / 6;
					System.out.println(entry.getKey() + "               " + marks.getSubject1() + "         "
							+ marks.getSubject2() + "         " + marks.getSubject3() + "       " + marks.getSubject4()
							+ "         " + marks.getSubject5() + "         " + marks.getSubject6() + "          "
							+ average + "%       " + marks.getSemBacklogs());
					System.out.println("------------------------------------------------------------------------------------------------------------");
				}

			}

			else {
				System.out.println("No marks added yet ,Please request admin to update your marks ");
			}
		}

	}
	
	/**
	 * this method returns the Webtarget obj which is connected to the server with base url
	 * need to add the reamining path to the base url
	 * @return Webtarget
	 */
	public WebTarget getWebTarget() {

	    ClientConfig config = new ClientConfig();  
	    Client client = ClientBuilder.newClient(config);  
	    WebTarget target = client.target(getBaseURI());
	    return target;
	}
	
	private static URI getBaseURI() {  
	    //here server is running on 4444 port number and project name is restfuljersey  
	    return UriBuilder.fromUri("http://localhost:8080/StudentRegistrationServer/rest/").build();  
	  }  
	
}
