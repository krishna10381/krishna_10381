package com.student_registration.utilities;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.student_registration.models.AdminModel;


public  class JsonDeserializer {
	
	/**
	 * this method coverts the given json String into 
	 * @param jsonString
	 * @return
	 */
	public static <T> T getObject(String jsonString,Class<T> type){
	   
		try{
			Gson gson = new Gson(); 
	       T obj = gson.fromJson(jsonString,type);
	       return obj;
		}catch (Exception e) {
			return null;
		}
	} 
	
	public static <T> Map<String,T> getMap(String jsonString,Class<T> type){
		   Gson gson = new Gson(); 
		   Type mapType = new TypeToken<Map<String, T>>(){}.getType();	
		   Map<String,T> obj = gson.fromJson(jsonString,mapType);
		   return obj ;
		}
	public static <T> ArrayList<T> getList(String jsonString,Class<T[]> type){
		
		    T[] arr = new Gson().fromJson(jsonString, type);
		    return new ArrayList<T>(Arrays.asList(arr)); //or return Arrays.asList(new Gson().fromJson(s, clazz)); for a one-liner
		}
		

}
