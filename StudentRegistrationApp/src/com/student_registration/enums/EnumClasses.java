package com.student_registration.enums;

public class EnumClasses {
	public static enum SearchKeys{
	    ID,
		ALL,
		NAME,
		COURSE,
		DUE,
		CAST,
		YEAR,
		BACKLOGS,
		GENDER
	}
}
