package com.springcore.bean;

public class Employee {
	private int id;
	private String name;
	private Address address;

	public Employee(int id, String name, Address address) {
		this.id = id;
		this.address = address;
		this.name = name;

	}

}
