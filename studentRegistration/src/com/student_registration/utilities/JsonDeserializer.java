package com.student_registration.utilities;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.student_registration.models.AdminModel;

public  class JsonDeserializer {
	
	/**
	 * this method coverts the given json String into 
	 * @param jsonString
	 * @return
	 */
	public static <T> T getObject(String jsonString,Class<T> type){
	   Gson gson = new Gson(); 
	   T obj = gson.fromJson(jsonString,type);
	   return obj;
	}

}
