package com.springcore.Main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.springcore.bean.Employee;

/**
 * Hello world!
 *
 */
public class SpringCoreMain {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("com/springcore/config/EmployeeConfig.xml");
		Employee employee = (Employee) context.getBean("employee");
		employee.getName();
		System.out.println(employee.getId() + "\t" + employee.getName());
		System.out.println(employee.getAddress().getCity()+"\t"+employee.getAddress().getState());
	}
}
