package com.abc.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.abc.hibernate.entities.Account;
import com.abc.service.AccountService;

@Controller
public class AccountController {

	@Autowired
	private AccountService accountService;

	@GetMapping("search/{id}")
	public String searchAccount(@PathVariable("id") int accno, ModelMap map) {
		Account account = accountService.searchAccountByID(accno);
		map.addAttribute("account", account);
		System.out.println(account.getName());
		return "account_saved";
	}

	@PostMapping("/create")
	public String createAccount(@ModelAttribute Account acc, ModelMap map) {
		String modelData;
		System.out.println("create");
		if (accountService.createAccount(acc)) {
			modelData = "Account Created Successfully";
		} else {
			modelData = "Account Not Created";
		}
		map.addAttribute("modelData", modelData);
		return "message";

	}

	@GetMapping("/Delete")
	public String deleteAccount(@RequestParam int accno, ModelMap map) {
		String modelData;
		if (accountService.deleteAccountByID(accno)) {
			modelData = "Account Deleted Successfully";
		} else {
			modelData = "Account not Deleted";
		}
		map.addAttribute("modelData", modelData);
		return "message";

	}

	
	@PostMapping("/update")
	public String updateAccount(@ModelAttribute Account account, ModelMap map) {
		String modelData;
		if (accountService.updateAccount(account)) {
			modelData = "Account Updated Successfully";
		} else {
			modelData = "Account not updated";
		}
		map.addAttribute("modelData", modelData);
		return "message";

	}

}
