package com.abc.service;

import com.abc.hibernate.entities.Account;

public interface AccountService {

	Account searchAccountByID(int accno);

	public boolean createAccount(Account account);

	public boolean deleteAccountByID(int accno);

	public boolean updateAccount(Account account);

}
