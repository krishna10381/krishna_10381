package com.abc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.abc.dao.AccountDAO;
import com.abc.hibernate.entities.Account;

@Service
public class AccountServieImpl implements AccountService {

	@Autowired
	private AccountDAO accountDAO;

	@Transactional
	@Override
	public Account searchAccountByID(int accno) {
		// TODO Auto-generated method stub
		return accountDAO.getAccountByID(accno);
	}

	@Transactional
	@Override
	public boolean createAccount(Account account) {
		// TODO Auto-generated method stub
		return accountDAO.createAccount(account);
	}

	@Transactional
	@Override
	public boolean deleteAccountByID(int accno) {
		// TODO Auto-generated method stub
		return accountDAO.deleteAccount(accno);
	}
	@Transactional
	@Override
	public boolean updateAccount(Account account) {
		// TODO Auto-generated method stub
		return accountDAO.updateAccount(account);
	}

}
