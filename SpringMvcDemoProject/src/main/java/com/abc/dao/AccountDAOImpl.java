package com.abc.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.abc.hibernate.entities.Account;

@Repository
public class AccountDAOImpl implements AccountDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Account getAccountByID(int accno) {
		Session session = sessionFactory.getCurrentSession();
		Account account = session.get(Account.class, accno);
		return account;
	}

	@Override
	public boolean createAccount(Account account) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		session.save(account);

		return true;

	}

	@Override
	public boolean deleteAccount(int accno) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		Account account = session.get(Account.class, accno);
		if (account != null) {
			session.delete(account);
			return true;
		} else {
			return false;
		}

	}
	
	@Override
	public boolean updateAccount(Account account) {
		Session session = sessionFactory.getCurrentSession();
		Account acc = session.get(Account.class, account.getAccno());
		if (acc != null) {
			session.merge(acc);
			return true;
		} else {
			return false;
		}
	}

}
