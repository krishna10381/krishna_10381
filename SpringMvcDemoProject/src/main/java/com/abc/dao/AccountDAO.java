package com.abc.dao;

import com.abc.hibernate.entities.Account;

public interface AccountDAO {

	Account getAccountByID(int accno);

	boolean createAccount(Account account);

	boolean deleteAccount(int accno);

	boolean updateAccount(Account account);
}
